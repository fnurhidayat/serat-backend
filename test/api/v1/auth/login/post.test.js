const faker = require("faker");
const app = require("../../../../../app");
const request = require("supertest");
const { createUser } = require("../../../../utils");

describe("POST /api/v1/auth/login", () => {
  it("should respond with 201", async () => {
    const user = await createUser();

    return request(app)
      .post("/api/v1/auth/login")
      .send({
        username: user.username,
        password: "123456",
      })
      .then((res) => {
        expect(res.statusCode).toEqual(201);
        expect(res.body.data.token).toEqual(expect.any(String));
      });
  });

  it("should respond with 401", async () => {
    const username = faker.internet.userName();

    return request(app)
      .post("/api/v1/auth/login")
      .send({
        username: username,
        password: "123456",
      })
      .then((res) => {
        expect(res.statusCode).toEqual(401);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            name: "CredentialError",
            message: `User with username=${username} does not exist!`,
          })
        );
      });
  });

  it("should respond with 401", async () => {
    const user = await createUser();

    return request(app)
      .post("/api/v1/auth/login")
      .send({
        username: user.username,
        password: "123457",
      })
      .then((res) => {
        expect(res.statusCode).toEqual(401);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            name: "CredentialError",
            message: "Wrong password!",
          })
        );
      });
  });
});
