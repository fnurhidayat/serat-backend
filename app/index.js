const express = require("express");
const configureServer = require("./config/server");
const app = express();

const { PORT = 8000 } = process.env;

const server = configureServer(app);

module.exports = server;
