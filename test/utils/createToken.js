const faker = require("faker");
const { sign } = require("jsonwebtoken");
const { JWT_SIGNATURE_KEY } = require("../../app/config/environment");
const { User } = require("../../app/models");

module.exports = async function createToken(user) {
  if (!user)
    user = await User.create({
      username: faker.internet.userName(),
      encryptedPassword:
        "$2a$10$2NqqtMCVwL7uSM7YJJ77oOCRuL59gQsS9zrntklHqPoNASczL3hM6",
    });

  const token = sign(
    {
      id: user.id,
      username: user.username,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    },
    JWT_SIGNATURE_KEY
  );

  return token;
};
