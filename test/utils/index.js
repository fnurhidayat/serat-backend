const createSerat = require("./createSerat");
const createToken = require("./createToken");
const createUser = require("./createUser");

module.exports = {
  createSerat,
  createToken,
  createUser,
};
