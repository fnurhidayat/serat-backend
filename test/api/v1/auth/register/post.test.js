const faker = require("faker");
const app = require("../../../../../app");
const request = require("supertest");
const { createUser } = require("../../../../utils");

describe("POST /api/v1/auth/register", () => {
  it("should respond with 201", () => {
    return request(app)
      .post("/api/v1/auth/register")
      .send({
        username: faker.internet.userName(),
        password: "123456",
      })
      .then((res) => {
        expect(res.statusCode).toEqual(201);
        expect(res.body.data.token).toEqual(expect.any(String));
      })
  });

  it("should respond with 422", async () => {
    const user = await createUser();

    return request(app)
      .post("/api/v1/auth/register")
      .send({
        username: user.username,
        password: "123457",
      })
      .then((res) => {
        expect(res.statusCode).toEqual(422);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            name: "Error",
            message: "username already taken!",
          })
        );
      });
  });
});
