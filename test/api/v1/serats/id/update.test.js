const faker = require("faker");
const app = require("../../../../../app");
const request = require("supertest");
const { createSerat, createToken, createUser } = require("../../../../utils");

describe("PUT /api/v1/serats/:id", () => {
  it("should respond with 201 as status code when parameters are valid", async () => {
    const user = await createUser();
    const token = await createToken(user);
    const serat = await createSerat(user);

    return request(app)
      .put(`/api/v1/serats/${serat.id}`)
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + token)
      .send({
        title: faker.lorem.words(),
        body: faker.lorem.paragraphs(),
        publishedAt: new Date(),
      })
      .then((res) => {
        expect(res.statusCode).toEqual(200);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            title: expect.any(String),
            body: expect.any(String),
            createdAt: expect.any(String),
            updatedAt: expect.any(String),
            publishedAt: expect.any(String),
          })
        );
      });
  });

  it("should respond with 403 as status code when parameters are valid", async () => {
    const user = await createUser();
    const token = await createToken(user);
    const serat = await createSerat();

    return request(app)
      .put(`/api/v1/serats/${serat.id}`)
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + token)
      .send({
        title: faker.lorem.words(),
        body: faker.lorem.paragraphs(),
        publishedAt: new Date(),
      })
      .then((res) => {
        expect(res.statusCode).toEqual(403);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            name: "ForbiddenError",
            message: "cannot modify someone else's serat!",
          })
        );
      });
  });

  it("should respond with 404", async () => {
    const token = await createToken();

    return request(app)
      .put("/api/v1/serats/sesuatu")
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + token)
      .send({
        title: faker.lorem.words(),
        body: faker.lorem.paragraphs(),
        publishedAt: new Date(),
      })
      .then((res) => {
        expect(res.statusCode).toEqual(404);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            name: "SeratNotFoundError",
            message: "serat with id=sesuatu does not exist!",
          })
        );
      });
  });

  it("should respond with 422 as status code when parameters are not valid", async () => {
    const user = await createUser();
    const token = await createToken(user);
    const serat = await createSerat(user);

    return request(app)
      .put(`/api/v1/serats/${serat.id}`)
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + token)
      .send({
        title: 1,
        body: faker.lorem.paragraphs(),
        publishedAt: new Date(),
      })
      .then((res) => {
        expect(res.statusCode).toEqual(422);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            name: "Error",
            message: "title must be a string!",
            payload: {
              id: expect.any(String),
              title: 1,
              body: expect.any(String),
              publishedAt: expect.any(String),
            },
          })
        );
      });
  });
});
