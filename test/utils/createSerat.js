const faker = require("faker");
const { Serat, User } = require("../../app/models");

module.exports = async function createSerat(user) {
  if (!user)
    user = await User.create({
      username: faker.internet.userName(),
      encryptedPassword:
        "$2a$10$2NqqtMCVwL7uSM7YJJ77oOCRuL59gQsS9zrntklHqPoNASczL3hM6",
    });

  return Serat.create({
    title: faker.lorem.words(),
    body: faker.lorem.paragraphs(),
    publishedAt: new Date(),
    userId: user.id,
  });
};
