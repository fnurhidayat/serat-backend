class Model {
  static data = {};

  static get table() {
    return this.name.toLowerCase();
  }

  static get records() {
    return this.data[this.table] || [];
  }

  static set records(value) {
    this.data[this.table] = value;
  }

  static generateId() {
    const sortedRows = this.records.sort((a, b) => a.id - b.id);
    return sortedRows[sortedRows.length - 1]?.id + 1 || 1;
  }

  static async create(payload) {
    const timestamp = new Date();

    const record = {
      id: this.generateId(),
      ...payload,
      createdAt: timestamp,
      updatedAt: timestamp,
    };

    if (this.records.length === 0) this.records = [record];
    else this.records.push(record);

    return new this(record);
  }

  static async getAll({
    page = 1,
    pageSize = 10,
    filter = () => true,
  } = {}) {
    const sidx = (page - 1) * pageSize;
    const eidx = page * pageSize;
    const data = this.records.filter(filter)
    return [
      // FIXME: Not effective
      data.slice(sidx, eidx).map((i) => new this(i)),
      {
        page,
        pageSize,
        pageCount: Math.ceil(data.length / pageSize),
      },
    ];
  }

  static async getById(id) {
    return this.records.find((i) => i.id === Number(id));
  }

  static async update(id, payload) {
    const targetIndex = this.records.findIndex((i) => i.id === Number(id));

    this.records[targetIndex] = {
      ...this.records[targetIndex],
      ...payload,
      updatedAt: new Date(),
    };

    return new this(this.records[targetIndex]);
  }

  static async destroy(id) {
    if (isNaN(id)) throw new Error("id must be a valid number!");
    this.records = this.records.filter((i) => i.id !== Number(id));
  }
}

module.exports = Model;
