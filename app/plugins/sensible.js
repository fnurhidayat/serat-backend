/* istanbul ignore file */
function handleRouteNotFound(req, res) {
  res.fail(404, {
    name: "RouteNotFoundError",
    message: `${req.method} ${req.url} is not found!`,
    url: req.url,
    method: req.method,
  });
}

function handleException(err, req, res, next) {
  res.error(500, err);
}

module.exports = { handleRouteNotFound, handleException };
