const Model = require("./model");

class User extends Model {
  constructor({ id, username, encryptedPassword, createdAt, updatedAt }) {
    super();
    this.id = id;
    this.username = username;
    this.encryptedPassword = encryptedPassword;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  static async create({ username, encryptedPassword }) {
    if (typeof username !== "string")
      throw new Error("username must be a string!");
    if (typeof encryptedPassword !== "string")
      throw new Error("encryptedPassword must be a string!");

    const user = await this.getByUsername(username.toLowerCase());
    if (!!user) throw new Error("username already taken!");

    return super.create({
      username: username.toLowerCase(),
      encryptedPassword,
    });
  }

  static async update(id, { username, encryptedPassword }) {
    if (isNaN(id)) throw new Error("id must be a valid number!");
    if (typeof username !== "string")
      throw new Error("username must be a string!");
    if (typeof encryptedPassword !== "string")
      throw new Error("encryptedPassword must be a string!");

    return super.update(id, {
      username,
      encryptedPassword,
    });
  }

  static async getByUsername(username) {
    if (typeof username !== "string")
      throw new Error("username must be a string!");

    return this.records.find((record) => record.username === username);
  }
}

module.exports = User;
