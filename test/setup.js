const { Serat, User } = require("../app/models");
const serats = require("../data/serats.json");
const users = require("../data/users.json");

module.exports = () => {
  Serat.records.push(
    ...serats.map((serat) => ({
      id: serat.id,
      title: serat.title,
      body: serat.body,
      userId: serat.userId,
      createdAt: new Date(serat.createdAt),
      updatedAt: new Date(serat.updatedAt),
      publishedAt: new Date(serat.publishedAt),
    }))
  );

  User.records.push(...users.map((user) => ({ ...user })));
};
