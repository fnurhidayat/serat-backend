const { verify } = require("jsonwebtoken");
const { JWT_SIGNATURE_KEY } = require("../config/environment");
const { User } = require("../models");

module.exports = async function authorize(req, res, next) {
  try {
    const token = req.headers.authorization.replace("Bearer ", "");
    const { id } = verify(token, JWT_SIGNATURE_KEY);
    const user = await User.getById(id);

    if (!user) throw new Error("access token revoked!");

    req.user = user;
  } catch (err) {
    res.fail(401, {
      name: err.name,
      message: err.message,
    });
    return;
  } finally {
    next();
  }
};
