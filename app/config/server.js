const express = require("express");
const cors = require("cors");
const bootstrap = require("./bootstrap");
const { ok, fail, error } = require("../plugins/jsend");
const { handleRouteNotFound, handleException } = require("../plugins/sensible");
const log = require("../plugins/log");
const echoRequest = require("../plugins/echoRequest");
const router = require("../router");

function configureServer(server) {
  server.use(cors());
  server.use(express.json());
  server.use(echoRequest);
  server.use(router);
  server.use(handleRouteNotFound);
  server.use(handleException);

  server.request.log = log;
  server.response.ok = ok;
  server.response.fail = fail;
  server.response.error = error;
  server.log = log;

  bootstrap(server);

  return server;
}

module.exports = configureServer;
