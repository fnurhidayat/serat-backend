const Serat = require("./serat");
const User = require("./user");

module.exports = {
  User,
  Serat,
};
