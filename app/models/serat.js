const Model = require("./model");
const User = require("./user");

class Serat extends Model {
  constructor({ id, title, body, createdAt, updatedAt, publishedAt, userId }) {
    super();
    this.id = id;
    this.title = title;
    this.body = body;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.publishedAt = publishedAt;
    this.userId = userId;
  }

  static async getAll({ userId, page, pageSize } = {}) {
    const [serats, pagination] = await super.getAll({
      page,
      pageSize,
      filter: (serat) => !userId || serat.userId === Number(userId),
    });

    // Eager loading
    const users = [];

    return [
      await Promise.all(
        serats.map(async (serat) => {
          const user =
            users.find((i) => i.id === serat.userId) ||
            (await User.getById(serat.userId));
          serat.userName = user.username;
          return serat;
        })
      ),
      pagination,
    ];
  }

  static async getById(id) {
    const serat = await super.getById(id);

    if (!serat) return null;

    const user = await User.getById(serat.userId);
    serat.userName = user.username;

    return serat;
  }

  static async create({ title, body, publishedAt, userId }) {
    if (typeof title !== "string") throw new Error("title must be a string!");
    if (typeof body !== "string") throw new Error("body must be a string!");
    if (!(publishedAt instanceof Date) && typeof publishedAt !== "undefined")
      throw new Error("publishedAt must be a date!");

    return super.create({
      title,
      body,
      publishedAt,
      userId,
    });
  }

  static async update(id, { title, body, publishedAt }) {
    if (isNaN(id)) throw new Error("id must be a valid number!");
    if (typeof title !== "string") throw new Error("title must be a string!");
    if (typeof body !== "string") throw new Error("body must be a string!");
    if (!(publishedAt instanceof Date) && typeof publishedAt !== "undefined")
      throw new Error("publishedAt must be a date!");

    return super.update(id, {
      title,
      body,
      publishedAt,
    });
  }
}

module.exports = Serat;
