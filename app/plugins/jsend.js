/* istanbul ignore file */
const status = {
  OK: "OK",
  FAIL: "FAIL",
  ERROR: "ERROR",
};

function ok(code, data, meta) {
  this.status(code).json({
    status: status.OK,
    meta,
    data,
  });
}

function fail(code, data) {
  this.status(code).json({
    status: status.FAIL,
    data,
  });
}

function error(code, err) {
  this.status(code).json({
    status: status.ERROR,
    data: {
      name: err.name,
      message: err.message,
      stack: err.stack,
    },
  });
}

module.exports = {
  ok,
  fail,
  error,
};
