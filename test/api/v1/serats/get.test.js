const faker = require("faker");
const app = require("../../../../app");
const request = require("supertest");
const createSerat = require("../../../utils/createSerat");

describe("GET /api/v1/serats", () => {
  it("should respond with 200", async () => {
    await createSerat();

    return request(app)
      .get("/api/v1/serats")
      .then((res) => {
        expect(res.statusCode).toEqual(200);
        expect(res.body.data.serats).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              title: expect.any(String),
              body: expect.any(String),
              createdAt: expect.any(String),
              updatedAt: expect.any(String),
              publishedAt: expect.any(String),
            }),
          ])
        );
      });
  });

  it("should respond with 200", async () => {
    await createSerat();

    return request(app)
      .get("/api/v1/serats?page=1&pageSize=10")
      .then((res) => {
        expect(res.statusCode).toEqual(200);
        expect(res.body.data.serats).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              title: expect.any(String),
              body: expect.any(String),
              createdAt: expect.any(String),
              updatedAt: expect.any(String),
              publishedAt: expect.any(String),
            }),
          ])
        );
      });
  });
});
