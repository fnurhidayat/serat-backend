const { User, Serat } = require("../models");
const serats = require("../../data/serats.json");
const users = require("../../data/users.json");

module.exports = async function boostrap(server) {
  server.log.debug("run bootstrap function");
  const userIds = users.map((i) => i.id);

  await Promise.all(
    serats.map((serat) =>
      Serat.create({
        title: serat.title,
        body: serat.body,
        publishedAt: new Date(serat.publishedAt),
        userId: userIds[Math.floor(Math.random() * userIds.length)],
      })
    )
  );

  await Promise.all(
    users.map((user) =>
      User.create({
        username: user.username,
        encryptedPassword: user.encryptedPassword,
      })
    )
  );
};
