const faker = require("faker");
const app = require("../../../../../app");
const request = require("supertest");
const createSerat = require("../../../../utils/createSerat");

describe("GET /api/v1/serats/:id", () => {
  it("should respond with 200", async () => {
    const serat = await createSerat();

    return request(app)
      .get(`/api/v1/serats/${serat.id}`)
      .then((res) => {
        expect(res.statusCode).toEqual(200);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            title: expect.any(String),
            body: expect.any(String),
            createdAt: expect.any(String),
            updatedAt: expect.any(String),
            publishedAt: expect.any(String),
          })
        );
      });
  });

  it("should respond with 404", async () => {
    await createSerat();

    return request(app)
      .get("/api/v1/serats/0")
      .then((res) => {
        expect(res.statusCode).toEqual(404);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            name: "SeratNotFoundError",
            message: "Serat with id=0 not found!",
            payload: {
              id: "0",
            },
          })
        );
      });
  });
});
