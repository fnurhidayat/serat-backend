const app = require("../../../../../app");
const request = require("supertest");
const { createToken } = require("../../../../utils");

describe("GET /api/v1/auth/whoami", () => {
  it("should respond with 200", async () => {
    const token = await createToken();

    return request(app)
      .get("/api/v1/auth/whoami")
      .set("Authorization", "Bearer " + token)
      .then((res) => {
        expect(res.statusCode).toEqual(200);
        expect(res.body.data).toEqual(expect.any(Object));
      });
  });

  it("should respond with 401", () =>
    request(app)
      .get("/api/v1/auth/whoami")
      .then((res) => {
        expect(res.statusCode).toEqual(401);
      })
  )

  it("should respond with 401", async () => {
    const token = await createToken({
      id: 0,
    });

    return request(app)
      .get("/api/v1/auth/whoami")
      .set("Authorization", "Bearer " + token)
      .then((res) => {
        expect(res.statusCode).toEqual(401);
      })
  })
});
