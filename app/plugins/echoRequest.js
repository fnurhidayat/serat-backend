/* istanbul ignore file */
const onFinished = require("on-finished");

function echoRequest(req, res, next) {
  onFinished(res, () => {
    const payload = {
      ...req.body,
      ...req.query,
      ...req.params,
    };

    delete payload.password;
    req.log.debug(res.statusCode, req.method, req.url, payload);
  });

  next();
}

module.exports = echoRequest;
