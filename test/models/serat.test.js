const faker = require("faker");
const { Serat, User } = require("../../app/models");

describe("Serat", () => {
  describe(".create", () => {
    it("should return new Serat", async () => {
      const user = await User.create({
        username: "jayabaya",
        encryptedPassword:
          "$2a$10$eQcr.k.VoYP/lL.7mMBGmOyswercKrSJBOSiA//ptRfl9.ZzeljCC",
      });

      return Serat.create({
        title: faker.lorem.words(),
        body: faker.lorem.paragraphs(),
        publishedAt: new Date(),
        userId: user.id,
      }).then((serat) => {
        expect(serat).toBeInstanceOf(Serat);
        expect(serat).toEqual(
          expect.objectContaining({
            title: expect.any(String),
            body: expect.any(String),
            publishedAt: expect.any(Date),
          })
        );
      });
    });

    it("should return new Serat and generate new id", async () => {
      const user = await User.create({
        username: "ryan",
        encryptedPassword:
          "$2a$10$eQcr.k.VoYP/lL.7mMBGmOyswercKrSJBOSiA//ptRfl9.ZzeljCC",
      });

      const serat = await Serat.create({
        title: faker.lorem.words(),
        body: faker.lorem.paragraphs(),
        publishedAt: new Date(),
        userId: user.id,
      });

      return Serat.create({
        title: faker.lorem.words(),
        body: faker.lorem.paragraphs(),
        publishedAt: new Date(),
        userId: User.records[0].id,
      }).then((newSerat) => {
        expect(newSerat.id).toEqual(serat.id + 1);
      });
    });

    it("should throw 'title must be a string!' when title is not a string", () => {
      const createSeratPromise = Serat.create({
        title: 1,
        body: faker.lorem.paragraphs(),
        publishedAt: new Date(),
      });

      return expect(createSeratPromise).rejects.toThrow(
        "title must be a string!"
      );
    });

    it("should throw 'body must be a string!' when body is not a string", () => {
      const createSeratPromise = Serat.create({
        title: faker.lorem.words(),
        body: 1,
        publishedAt: new Date(),
      });

      return expect(createSeratPromise).rejects.toThrow(
        "body must be a string!"
      );
    });

    it("should throw 'publishedAt must be a date!' when publishedAt is not a date", () => {
      const createSeratPromise = Serat.create({
        title: faker.lorem.words(),
        body: faker.lorem.paragraphs(),
        publishedAt: 1,
      });

      return expect(createSeratPromise).rejects.toThrow(
        "publishedAt must be a date!"
      );
    });
  });

  describe(".getAll", () => {
    it("should return array of Serat", async () => {
      return Serat.getAll().then(([serats, pagination]) => {
        expect(serats).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              title: expect.any(String),
              body: expect.any(String),
              createdAt: expect.any(Date),
              updatedAt: expect.any(Date),
              publishedAt: expect.any(Date),
            }),
          ])
        );

        expect(pagination).toEqual(
          expect.objectContaining({
            page: 1,
            pageSize: 10,
            pageCount: expect.any(Number),
          })
        );
      });
    });

    it("should return array of Serat that has userId equals to userId", async () => {
      return Serat.getAll({ userId: 1 }).then(([serats, pagination]) => {
        expect(serats).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              title: expect.any(String),
              body: expect.any(String),
              userId: 1,
              createdAt: expect.any(Date),
              updatedAt: expect.any(Date),
              publishedAt: expect.any(Date),
            }),
          ])
        );

        expect(pagination).toEqual(
          expect.objectContaining({
            page: 1,
            pageSize: 10,
            pageCount: expect.any(Number),
          })
        );
      });
    });
  });

  describe(".getById", () => {
    it("should return instance of Serat", async () => {
      return Serat.getById(1).then((serat) => {
        expect(serat).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            title: expect.any(String),
            body: expect.any(String),
            createdAt: expect.any(Date),
            updatedAt: expect.any(Date),
            publishedAt: expect.any(Date),
          })
        );
      });
    });

    it("should return null", async () => {
      return Serat.getById(0).then((serat) => {
        expect(serat).toEqual(null);
      });
    });
  });

  describe(".update", () => {
    it("should return instance of Serat", async () => {
      return Serat.update(1, {
        title: faker.lorem.words(),
        body: faker.lorem.paragraphs(),
        publishedAt: new Date(),
      }).then((serat) => {
        expect(serat).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            title: expect.any(String),
            body: expect.any(String),
            createdAt: expect.any(Date),
            updatedAt: expect.any(Date),
            publishedAt: expect.any(Date),
          })
        );
      });
    });

    it("should throw 'id must be a valid number' when id is not a valid number", async () => {
      return expect(Serat.update("angka", {})).rejects.toThrow(
        "id must be a valid number"
      );
    });

    it("should throw 'title must be a string!' when title is not a string", () => {
      const updateSeratPromise = Serat.update(1, {
        title: 1,
        body: faker.lorem.paragraphs(),
        publishedAt: new Date(),
      });

      return expect(updateSeratPromise).rejects.toThrow(
        "title must be a string!"
      );
    });

    it("should throw 'body must be a string!' when body is not a string", () => {
      const updateSeratPromise = Serat.update(1, {
        title: faker.lorem.words(),
        body: 1,
        publishedAt: new Date(),
      });

      return expect(updateSeratPromise).rejects.toThrow(
        "body must be a string!"
      );
    });

    it("should throw 'publishedAt must be a date!' when publishedAt is not a date", () => {
      const updateSeratPromise = Serat.update(1, {
        title: faker.lorem.words(),
        body: faker.lorem.paragraphs(),
        publishedAt: 1,
      });

      return expect(updateSeratPromise).rejects.toThrow(
        "publishedAt must be a date!"
      );
    });
  });

  describe(".delete", () => {
    it("should return undefined", async () => {
      return Serat.destroy(1).then((serat) => {
        expect(serat).toEqual(undefined);
        expect(Serat.records.find((i) => i.id === 1)).toEqual(undefined);
      });
    });

    it("should throw 'id must be a valid number' when id is not a valid number", async () => {
      return expect(Serat.destroy("angka")).rejects.toThrow(
        "id must be a valid number"
      );
    });
  });
});
