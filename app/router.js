const express = require("express");
const router = express.Router();
const {
  handleGetSerats,
  handlePostSerat,
  handleGetSeratById,
  handlePutSeratById,
  handleDeleteSeratById,
  handlePostLogin,
  handlePostRegister,
  handleGetWhoAmI,
} = require("./controllers");
const { authorize } = require("./middlewares");

router.post("/api/v1/auth/register", handlePostRegister);
router.post("/api/v1/auth/login", handlePostLogin);
router.get("/api/v1/auth/whoami", authorize, handleGetWhoAmI);

router.get("/api/v1/serats", handleGetSerats);
router.post("/api/v1/serats", authorize, handlePostSerat);
router.get("/api/v1/serats/:id", handleGetSeratById);
router.put("/api/v1/serats/:id", authorize, handlePutSeratById);
router.delete("/api/v1/serats/:id", authorize, handleDeleteSeratById);

module.exports = router;
