/* istanbul ignore file */
const { FgGreen, FgRed, FgBlue, FgYellow, Reset } = require("../lib/color");
const { LOG_LEVEL = 3, NODE_ENV = "development" } = process.env;

function info(...args) {
  if (NODE_ENV === "test") return;
  console.log(new Date(), `${FgGreen}info${Reset} [server]`, ...args);
}

function warn(...args) {
  if (NODE_ENV === "test") return;
  if (LOG_LEVEL < 2) return;
  console.log(new Date(), `${FgYellow}warn${Reset} [server]`, ...args);
}

function debug(...args) {
  if (NODE_ENV === "test") return;
  if (LOG_LEVEL < 3) return;
  console.log(new Date(), `${FgBlue}debug${Reset} [server]`, ...args);
}

function error(...args) {
  if (NODE_ENV === "test") return;
  console.log(new Date(), `${FgRed}error${Reset} [server]`, ...args);
}

module.exports = {
  info,
  debug,
  error,
  warn,
};
