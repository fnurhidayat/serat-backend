const authController = require("./authController");
const seratController = require("./seratController");

module.exports = {
  ...authController,
  ...seratController,
};
