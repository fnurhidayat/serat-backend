const { compareSync, hashSync } = require("bcryptjs");
const { sign } = require("jsonwebtoken");
const { JWT_SIGNATURE_KEY } = require("../config/environment");
const { User } = require("../models");

function createToken(payload) {
  return sign(payload, JWT_SIGNATURE_KEY);
}

exports.handlePostLogin = async function handlePostLogin(req, res) {
  const user = await User.getByUsername(req.body.username.toLowerCase());

  if (!user)
    return res.fail(401, {
      name: "CredentialError",
      message: `User with username=${req.body.username} does not exist!`,
      payload: {
        username: req.body.username,
      },
    });

  if (!compareSync(req.body.password, user.encryptedPassword))
    return res.fail(401, {
      name: "CredentialError",
      message: "Wrong password!",
      payload: {
        username: req.body.username,
      },
    });

  const token = createToken({
    id: user.id,
    username: user.username,
    createdAt: user.createdAt,
    updatedAt: user.updatedAt,
  });

  res.ok(201, {
    token,
  });
};

exports.handlePostRegister = async function handlePostRegister(req, res) {
  try {
    const { username, password } = req.body;
    const encryptedPassword = hashSync(password, 10);

    const user = await User.create({
      username,
      encryptedPassword,
    });

    const token = createToken({
      id: user.id,
      username: user.username,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    });

    res.ok(201, { token });
  } catch (err) {
    res.fail(422, {
      name: err.name,
      message: err.message,
      payload: {
        username: req.body.username,
      },
    });
  }
};

exports.handleGetWhoAmI = function handleGetWhoAmI(req, res) {
  res.ok(200, req.user);
};
