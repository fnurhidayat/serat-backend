const faker = require("faker");
const { User } = require("../../app/models");

module.exports = async function createUser() {
  const user = await User.create({
    username: faker.internet.userName(),
    encryptedPassword:
      "$2a$10$2NqqtMCVwL7uSM7YJJ77oOCRuL59gQsS9zrntklHqPoNASczL3hM6",
  });

  return user;
};
