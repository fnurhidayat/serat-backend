const { Serat } = require("../models");

exports.handleGetSerats = async function (req, res) {
  const [serats, pagination] = await Serat.getAll({
    page: Number(req.query.page || 1),
    pageSize: Number(req.query.pageSize || 10),
    userId: req.query.userId,
  });

  const data = {
    serats,
  };

  const meta = { pagination };

  res.ok(200, data, meta);
};

exports.handleGetSeratById = async function (req, res) {
  const serat = await Serat.getById(req.params.id);

  if (!serat)
    return res.fail(404, {
      name: "SeratNotFoundError",
      message: `Serat with id=${req.params.id} not found!`,
      payload: {
        ...req.params,
      },
    });

  res.ok(200, serat);
};

exports.handlePostSerat = async function (req, res) {
  try {
    const serat = await Serat.create({
      title: req.body.title,
      body: req.body.body,
      publishedAt: new Date(req.body.publishedAt),
      userId: req.user.id,
    });

    res.ok(201, serat);
  } catch (err) {
    res.fail(422, {
      name: err.name,
      message: err.message,
      payload: {
        ...req.body,
      },
    });
  }
};

exports.handlePutSeratById = async function (req, res) {
  try {
    const serat = await Serat.getById(Number(req.params.id));

    if (!serat) {
      res.fail(404, {
        name: "SeratNotFoundError",
        message: `serat with id=${req.params.id} does not exist!`,
        payload: {
          ...req.params,
          ...req.body,
        },
      });

      return;
    }

    if (serat.userId !== req.user.id) {
      res.fail(403, {
        name: "ForbiddenError",
        message: "cannot modify someone else's serat!",
        payload: {
          ...req.body,
          ...req.params,
        },
      });

      return;
    }

    await Serat.update(req.params.id, {
      id: Number(req.body.id),
      title: req.body.title,
      body: req.body.body,
      publishedAt: new Date(req.body.publishedAt),
    });

    res.ok(200, serat);
  } catch (err) {
    res.fail(422, {
      name: err.name,
      message: err.message,
      payload: {
        ...req.params,
        ...req.body,
      },
    });
  }
};

exports.handleDeleteSeratById = async function (req, res) {
  const serat = await Serat.getById(Number(req.params.id));

  if (!serat) {
    res.fail(404, {
      name: "SeratNotFoundError",
      message: `serat with id=${req.params.id} does not exist!`,
      payload: {
        ...req.params,
      },
    });

    return;
  }

  if (serat.userId !== req.user.id) {
    res.fail(403, {
      name: "ForbiddenError",
      message: "cannot delete someone else's serat!",
      payload: {
        ...req.body,
        ...req.query,
        ...req.params,
      },
    });

    return;
  }

  await Serat.destroy(Number(req.params.id));

  res.ok(204, null);
};
