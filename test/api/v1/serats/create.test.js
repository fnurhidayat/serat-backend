const faker = require("faker");
const app = require("../../../../app");
const request = require("supertest");
const createToken = require("../../../utils/createToken");

describe("POST /api/v1/serats", () => {
  it("should respond with 201 as status code when parameters are valid", async () => {
    const token = await createToken();
    return request(app)
      .post("/api/v1/serats")
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + token)
      .send({
        title: faker.lorem.words(),
        body: faker.lorem.paragraphs(),
        publishedAt: new Date(),
      })
      .then((res) => {
        expect(res.statusCode).toEqual(201);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            title: expect.any(String),
            body: expect.any(String),
            createdAt: expect.any(String),
            updatedAt: expect.any(String),
            publishedAt: expect.any(String),
          })
        );
      });
  });

  it("should respond with 422 as status code when parameters are not valid", async () => {
    const token = await createToken();
    return request(app)
      .post("/api/v1/serats")
      .set("Content-Type", "application/json")
      .set("Authorization", "Bearer " + token)
      .send({
        title: 1,
        body: faker.lorem.paragraphs(),
        publishedAt: new Date(),
      })
      .then((res) => {
        expect(res.statusCode).toEqual(422);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            name: "Error",
            message: "title must be a string!",
            payload: {
              title: 1,
              body: expect.any(String),
              publishedAt: expect.any(String),
            },
          })
        );
      });
  });
});
