const faker = require("faker");
const app = require("../../../../../app");
const request = require("supertest");
const { createSerat, createToken, createUser } = require("../../../../utils");

describe("DELETE /api/v1/serats/:id", () => {
  it("should respond with 204", async () => {
    const user = await createUser();
    const token = await createToken(user);
    const serat = await createSerat(user);

    return request(app)
      .delete(`/api/v1/serats/${serat.id}`)
      .set("Authorization", "Bearer " + token)
      .then((res) => {
        expect(res.statusCode).toEqual(204);
      });
  });

  it("should respond with 404", async () => {
    const user = await createUser();
    const token = await createToken(user);

    return request(app)
      .delete("/api/v1/serats/sesuatu")
      .set("Authorization", "Bearer " + token)
      .then((res) => {
        expect(res.statusCode).toEqual(404);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            name: "SeratNotFoundError",
            message: "serat with id=sesuatu does not exist!",
            payload: {
              id: "sesuatu",
            },
          })
        );
      });
  });

  it("should respond with 403", async () => {
    const user = await createUser();
    const token = await createToken();
    const serat = await createSerat(user);

    return request(app)
      .delete(`/api/v1/serats/${serat.id}`)
      .set("Authorization", "Bearer " + token)
      .then((res) => {
        expect(res.statusCode).toEqual(403);
        expect(res.body.data).toEqual(
          expect.objectContaining({
            name: "ForbiddenError",
            message: "cannot delete someone else's serat!",
            payload: {
              id: serat.id.toString(),
            },
          })
        );
      });
  });
});
