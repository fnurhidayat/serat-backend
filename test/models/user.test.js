const faker = require("faker");
const { User } = require("../../app/models");
const { createUser } = require("../utils");

describe("User", () => {
  describe(".create", () => {
    it("should return new User", () =>
      User.create({
        username: faker.internet.userName(),
        encryptedPassword:
          "$2a$10$eQcr.k.VoYP/lL.7mMBGmOyswercKrSJBOSiA//ptRfl9.ZzeljCC",
      }).then((user) => {
        expect(user).toBeInstanceOf(User);
        expect(user).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            username: expect.any(String),
            encryptedPassword: expect.any(String),
            createdAt: expect.any(Date),
            updatedAt: expect.any(Date),
          })
        );
      }));

    it("should throw 'username already taken!' when username already exists", async () => {
      const user = await createUser();

      return expect(
        User.create({
          username: user.username,
          encryptedPassword:
            "$2a$10$eQcr.k.VoYP/lL.7mMBGmOyswercKrSJBOSiA//ptRfl9.ZzeljCC",
        })
      ).rejects.toThrow("username already taken!");
    });

    it("should throw 'username must be a string!' when username is not a string", () => {
      const createUserPromise = User.create({
        username: 1,
        encryptedPassword:
          "$2a$10$eQcr.k.VoYP/lL.7mMBGmOyswercKrSJBOSiA//ptRfl9.ZzeljCC",
      });

      return expect(createUserPromise).rejects.toThrow(
        "username must be a string!"
      );
    });

    it("should throw 'encryptedPassword must be a string!' when encryptedPassword is not a string", () => {
      const createUserPromise = User.create({
        username: faker.internet.userName(),
        encryptedPassword: 1,
      });

      return expect(createUserPromise).rejects.toThrow(
        "encryptedPassword must be a string!"
      );
    });
  });

  describe(".getAll", () => {
    it("should return array of User", async () => {
      return User.getAll().then(([users, pagination]) => {
        expect(users).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: expect.any(Number),
              username: expect.any(String),
              encryptedPassword: expect.any(String),
              createdAt: expect.any(Date),
              updatedAt: expect.any(Date),
            }),
          ])
        );

        expect(pagination).toEqual(
          expect.objectContaining({
            page: 1,
            pageSize: 10,
            pageCount: expect.any(Number),
          })
        );
      });
    });
  });

  describe(".getById", () => {
    it("should return instance of User", async () => {
      return User.getById(1).then((user) => {
        expect(user).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            username: expect.any(String),
            encryptedPassword: expect.any(String),
            createdAt: expect.any(Date),
            updatedAt: expect.any(Date),
          })
        );
      });
    });

    it("should return null", async () => {
      return User.getById(0).then((user) => {
        expect(user).toEqual(undefined);
      });
    });
  });

  describe(".getByUsername", () => {
    it("should return instance of User", async () => {
      const user = await createUser();

      return User.getByUsername(user.username).then((user) => {
        expect(user).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            username: expect.any(String),
            encryptedPassword: expect.any(String),
            createdAt: expect.any(Date),
            updatedAt: expect.any(Date),
          })
        );
      });
    });

    it("should throw 'username must be a string!'", async () => {
      return expect(User.getByUsername(100)).rejects.toThrow(
        "username must be a string!"
      );
    });

    it("should return null", async () => {
      return User.getByUsername("norak").then((user) => {
        expect(user).toEqual(undefined);
      });
    });
  });

  describe(".update", () => {
    it("should return instance of User", async () => {
      return User.update(1, {
        username: faker.internet.userName(),
        encryptedPassword:
          "$2a$10$eQcr.k.VoYP/lL.7mMBGmOyswercKrSJBOSiA//ptRfl9.ZzeljCC",
      }).then((user) => {
        expect(user).toEqual(
          expect.objectContaining({
            id: expect.any(Number),
            username: expect.any(String),
            encryptedPassword: expect.any(String),
            createdAt: expect.any(Date),
            updatedAt: expect.any(Date),
          })
        );
      });
    });

    it("should throw 'id must be a valid number' when id is not a valid number", async () => {
      return expect(User.update("angka", {})).rejects.toThrow(
        "id must be a valid number"
      );
    });

    it("should throw 'username must be a string!' when username is not a string", () => {
      const updateUserPromise = User.update(1, {
        username: 1,
        encryptedPassword:
          "$2a$10$eQcr.k.VoYP/lL.7mMBGmOyswercKrSJBOSiA//ptRfl9.ZzeljCC",
      });

      return expect(updateUserPromise).rejects.toThrow(
        "username must be a string!"
      );
    });

    it("should throw 'encryptedPassword must be a string!' when encryptedPassword is not a string", () => {
      const updateUserPromise = User.update(1, {
        username: faker.internet.userName(),
        encryptedPassword: 1,
      });

      return expect(updateUserPromise).rejects.toThrow(
        "encryptedPassword must be a string!"
      );
    });
  });

  describe(".delete", () => {
    it("should return undefined", async () => {
      return User.destroy(1).then((user) => {
        expect(user).toEqual(undefined);
        expect(User.records.find((i) => i.id === 1)).toEqual(undefined);
      });
    });

    it("should throw 'id must be a valid number' when id is not a valid number", async () => {
      return expect(User.destroy("angka")).rejects.toThrow(
        "id must be a valid number"
      );
    });
  });
});
